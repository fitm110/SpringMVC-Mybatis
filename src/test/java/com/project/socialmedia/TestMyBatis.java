package com.project.socialmedia;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.socialmedia.entity.userEntity;
import com.project.socialmedia.service.userService;
import com.sun.istack.internal.logging.Logger;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:spring-mybatis.xml"})
public class TestMyBatis {
	private static Logger logger = Logger.getLogger(TestMyBatis.class);
	
	@Resource
	private userService userService;
	
	@Test
	public void test(){
		List<userEntity> result = userService.getUserInfo();
		final ObjectMapper mapper = new ObjectMapper();
		final StringWriter writer = new StringWriter();
		try {
			mapper.writeValue(writer, result);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		logger.info(writer.toString());
	}
}
