package com.project.socialmedia.controller;

import java.io.IOException;
import java.io.StringWriter;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.socialmedia.entity.userEntity;
import com.project.socialmedia.service.userService;


@RestController
@RequestMapping("/user")
public class userAction {
	@Resource
	private userService userService;
	
	@RequestMapping(value = "/info")
	public String testUrl(HttpServletRequest request, Model model){
		List<userEntity> userEntity = userService.getUserInfo(); 
		final ObjectMapper mapper = new ObjectMapper();
		final StringWriter writer = new StringWriter();
		try {
			mapper.writeValue(writer, userEntity);
		} catch (final IOException e) {
			e.printStackTrace();
		}
		model.addAttribute("Info_output", writer.toString());
		return "info";//which .jsp page returns (InternalResourceViewResolver)
	}
}
