package com.project.socialmedia.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.project.socialmedia.dao.userDao;
import com.project.socialmedia.entity.userEntity;

@Service
public class userServiceImpl implements userService {
	
	@Resource
	private userDao userDao;

	public List<userEntity> getUserInfo() {
		
		return userDao.getUser();
	}

}
